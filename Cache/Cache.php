<?php

namespace Samarties\CacheBundle\Cache;

use Samarties\Cacher\Driver\DriverInterface;
use Samarties\CacheBundle\Exception\CacheDriverNotSupportedException;

class Cache implements CacheInterface
{
	protected $driver;
	protected $enabled;
	protected $salt;
	
	/**
	 * Stores keys and/or tags for entries pending deletion. This queue should be
	 * cleared by an event listener later.
	 *
	 * @var array
	 */
	protected $deleteQueue;
	
	/**
	 * Constructor
	 * 
	 * @param string $driverClass
	 * @param string $salt
	 * 
	 * @throws CacheDriverNotSupportedException When the given driver is not supported by the system
	 */
	public function __construct($driverClass, $salt = '')
	{		
		$driver = new $driverClass();
		if (!$driver->isSupported())
		{
			throw new CacheDriverNotSupportedException(get_class($driver));
		}
		$this->driver = $driver;
		$this->enabled = true;
		$this->deleteQueue = array();
		$this->setSalt($salt);
	}
	
	/**
	 * @param string $salt
	 */
	public function setSalt($salt)
	{
		$this->salt = (string) $salt;
	}
	
	/**
	 * @return string
	 */
	public function getSalt()
	{
		return $this->salt;
	}
	
	/**
	 * @inheritDoc
	 */
	public function isEnabled()
	{
		return $this->enabled;
	}
	
	/**
	 * @inheritDoc
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled == true;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getDriver()
	{
		return $this->driver;
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return false
	 */
	public function save($key, $data, array $tags = array(), $lifeTime = 0)
	{
		if (!$this->isEnabled()) return false;
		
		return $this->driver->save($this->getSaltedKey($key), $data, $this->getSaltedTags($tags), $lifeTime);
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return false
	 */
	public function contains($key)
	{
		if (!$this->isEnabled()) return false;
		
		return $this->driver->contains($this->getSaltedKey($key));
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return null
	 */
	public function fetch($key)
	{
		if (!$this->isEnabled()) return null;
		
		return $this->driver->fetch($this->getSaltedKey($key));
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return false
	 */
	public function delete($key)
	{
		if (!$this->isEnabled()) return false;
		
		return $this->driver->delete($this->getSaltedKey($key));
	}
	
	/**
	 * @inheritDoc
	 *
	 * If the cache is disabled this function will simply return false
	 */
	public function deleteMultiple(array $keys)
	{
		if (!$this->isEnabled()) return false;
		
		return $this->driver->deleteMultiple($this->getSaltedKeys($keys));
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return null
	 */
	public function deleteWithTags(array $tags)
	{
		if (!$this->isEnabled()) return;
		
		$this->driver->deleteWithTags($this->getSaltedTags($tags));
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return null
	 */
	public function deleteByRegex($pattern)
	{
		if (!$this->isEnabled()) return;
		
		$this->driver->deleteMultiple($this->getListByRegex($pattern));
	}
	
	/**
	 * @inheritDoc
	 */
	public function queueDelete(CacheDeleteRequestInterface $CacheDeleteRequest)
	{
		$this->deleteQueue[] = $CacheDeleteRequest;
	}
	
	/**
	 * @inheritDoc
	 * 
	 * If the cache is disabled this function will simply return null
	 */
	public function runDeleteQueue()
	{
		if (!$this->isEnabled()) return;
		
		foreach($this->deleteQueue as $CacheDeleteRequest)
		{
			switch($CacheDeleteRequest->getMethod())
			{
				case CacheDeleteRequestInterface::DELETE_TAG:
					$this->deleteWithTags(array($CacheDeleteRequest->getReference()));
					break;
				case CacheDeleteRequestInterface::DELETE_REGEX:
					$this->deleteByRegex($CacheDeleteRequest->getReference());
					break;
				default:
					if ($this->contains($CacheDeleteRequest->getReference()))
					{
						$this->delete($CacheDeleteRequest->getReference());
					}
			}
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public function getDeleteQueue()
	{
		return $this->deleteQueue;
	}
	
	/**
	 * @inheritDoc
	 *
	 * If the cache is disabled this function will simply return false
	 */
	public function flush()
	{
		if (!$this->isEnabled()) return false;

		$this->driver->deleteMultiple($this->getList());

		return true;
	}
	
	/**
	 * @inheritDoc
	 * 
	 * @return array|false
	 */
	public function getList()
	{
		return $this->filterSaltedKeys($this->driver->getList());
	}
	
	/**
	 * @inheritDoc
	 *
	 * @return array|false
	 */
	public function getListWithTags(array $tags)
	{
		return $this->driver->getListWithTags($this->getSaltedTags($tags));
	}
	
	/**
	 * @inheritDoc
	 */
	public function getListByRegex($pattern)
	{
		return $this->filterSaltedKeys(($this->driver->getListByRegex($pattern)));
	}
	
	/**
	 * @inheritDoc
	 */
	public static function isSupported()
	{
		return true;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getSaltedKey($key)
	{
		return $key . ($this->getSalt() ? '_' . $this->getSalt() : '');
	}
	
	/**
	 * @inheritDoc
	 */
	public function getSaltedTag($tag)
	{
		return $tag . ($this->getSalt() ? '_' . $this->getSalt() : '');
	}
	
	/**
	 * Adds the cache's salt to an array of keys
	 *
	 * @param array $keys
	 */
	protected function getSaltedKeys(array $keys)
	{
		foreach($keys as &$key)
		{
			$key = $this->getSaltedKey($key);
		}
		unset($key);
	
		return $keys;
	}
	
	/**
	 * Adds the cache's salt to an array of tags
	 * 
	 * @param array $tags
	 */
	protected function getSaltedTags(array $tags)
	{
		foreach($tags as &$tag)
		{
			$tag = $this->getSaltedTag($tag);
		}
		unset($tag);
		
		return $tags;
	}
	
	/**
	 * Returns a regular expression which can be used to filter 
	 * salted keys from other keys
	 * 
	 * @return string
	 */
	protected function getSaltedKeysRegexPattern()
	{
		return sprintf('/_%s$/', $this->getSalt());
	}
	
	/**
	 * Returns a regular expression which can be used to filter
	 * salted tags from other tags
	 *
	 * @return string
	 */
	protected function getSaltedTagsRegexPattern()
	{
		return sprintf('/_%s$/', $this->getSalt());
	}
	
	/**
	 * Filters an array of keys so that it only contains our salted keys
	 * 
	 * @param array $keys
	 * 
	 * @return array
	 */
	protected function filterSaltedKeys(array $keys)
	{
		$filteredKeys = array();
		$saltPattern = $this->getSaltedKeysRegexPattern();
		foreach($keys as $key)
		{
			if (preg_match($saltPattern, $key))
			{
				$filteredKeys[] = $key;
			}
		}
		
		return $filteredKeys;
	}
}
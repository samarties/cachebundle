<?php

namespace Samarties\CacheBundle\Cache;

class CacheDeleteRequest implements CacheDeleteRequestInterface
{
	protected $method;
	protected $reference;
	
	/**
	 * Constructor
	 * 
	 * @param integer $method
	 * @param string $reference
	 */
	public function __construct($method, $reference)
	{
		$this->method = (int) $method;
		$this->reference = $reference;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getMethod()
	{
		return $this->method;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getReference()
	{
		return $this->reference;
	}
}
<?php

namespace Samarties\CacheBundle\Cache;

use Samarties\CacheBundle\Exception\CacheNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Samarties\CacheBundle\Exception\CacheDriverNotSupportedException;

class CacheManager
{
	protected $container;
	protected $cacheServiceIds;
	
	/**
	 * Constructor
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->cacheServiceIds = array();
	}
	
	/**
	 * @param string $cacheName
	 * @param string $cacheServiceId
	 */
	public function addCacheService($cacheName, $cacheServiceId)
	{
		$this->cacheServiceIds[$cacheName] = $cacheServiceId;
	}
	
	/**
	 * @return array<CacheInterface>
	 */
	public function getCaches()
	{
		$caches = array();
		foreach(array_keys($this->cacheServiceIds) as $cacheName)
		{
			$caches[$cacheName] = $this->getCache($cacheName);
		}
		
		return $caches;
	}
	
	/**
	 * @param string $cacheName
	 * 
	 * @return CacheInterface
	 */
	public function getCache($cacheName)
	{
		if (!isset($this->cacheServiceIds[$cacheName]))
		{
			throw new CacheNotFoundException($cacheName);
		}
		
		$cache = $this->container->get($this->cacheServiceIds[$cacheName]);
		$this->validateCache($cache);
		
		return $cache;
	}
	
	/**
	 * Ensures a cache is valid
	 * 
	 * @param CacheInterface $cache
	 * 
	 * @throws CacheDriverNotSupportedException When the cache driver is not supported by the system
	 */
	protected function validateCache(CacheInterface $cache)
	{
		if (!$cache->isSupported())
		{
			throw new CacheDriverNotSupportedException(get_class($cache->getDriver()));
		}
	}
}
<?php

namespace Samarties\CacheBundle\Cache;

interface CacheDeleteRequestInterface
{
	const DELETE_KEY = 0;
	const DELETE_TAG = 1;
	const DELETE_REGEX = 2;
	
	/**
	 * Determines what method to use for the delete
	 * 
	 * @return integer
	 */
	public function getMethod();
	
	/**
	 * The reference used to lookup entries for the delete
	 * 
	 * @return string
	 */
	public function getReference();
}
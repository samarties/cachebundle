<?php

/*
 * This file defines a salted cache interface. Any cache implementing this interface
 * is expected to work solely with its salted data. Any other data within the driver's control
 * should not be tampered with.
 */

namespace Samarties\CacheBundle\Cache;

use Samarties\Cacher\Driver\DriverInterface;

interface CacheInterface extends DriverInterface
{
	/**
	 * Determines if this cache is enabled
	 *
	 * @return boolean
	 */
	public function isEnabled();
	
	/**
	 * @param boolean $enabled
	 */
	public function setEnabled($enabled);
	
	/**
	 * @return DriverInterface
	 */
	public function getDriver();
	
	/**
	 * Stores a delete request in a queue, pending future deletion
	 * 
	 * @param CacheDeleteRequestInterface $CacheDeleteRequest
	 */
	public function queueDelete(CacheDeleteRequestInterface $CacheDeleteRequest);
	
	/**
	 * Steps through the deletion queue and executes each one
	 */
	public function runDeleteQueue();
	
	/**
	 * Returns an array of delete requests
	 * 
	 * @return array<CacheDeleteRequestInterface>
	 */
	public function getDeleteQueue();
	
	/**
	 * A salt can be given to make keys unique between multiple cache instances. Useful
	 * if two applications on a server use the same keys
	 * 
	 * @param string $salt
	 */
	public function setSalt($salt);
	
	/**
	 * Adds the cache's salt to the key and returns it
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	public function getSaltedKey($key);
	
	/**
	 * Adds the cache's salt to the tag and returns it
	 *
	 * @param string $tag
	 *
	 * @return string
	 */
	public function getSaltedTag($tag);
}
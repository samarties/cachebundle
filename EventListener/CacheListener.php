<?php

namespace Samarties\CacheBundle\EventListener;

use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Samarties\CacheBundle\Cache\CacheManager;

class CacheListener
{
	protected $cacheManager;
	
	/**
	 * Constructor
	 * 
	 * @param CacheManager $cacheManager
	 */
	public function __construct(CacheManager $cacheManager)
	{
		$this->cacheManager = $cacheManager;
	}
	
	/**
	 * @param PostResponseEvent $event
	 */
	public function runDeleteQueues(PostResponseEvent $event)
	{
		foreach($this->cacheManager->getCaches() as $cache)
		{
			$cache->runDeleteQueue();
		}
	}
}
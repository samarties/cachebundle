<?php

namespace Samarties\CacheBundle\Exception;

class CacheDriverNotSupportedException extends \Exception
{
	protected $message = 'The cache driver \'%s\' is not supported by your system.';
	
	public function __construct($driver, $code = 0, \Exception $previous = null)
	{
		parent::__construct(sprintf($this->message, $driver), $code, $previous);
	}
}
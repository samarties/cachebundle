<?php

namespace Samarties\CacheBundle\Exception;

class CacheNotFoundException extends \Exception
{
	protected $message = 'The cache \'%s\' could not be found.';
	
	public function __construct($cacheName, $code = 0, \Exception $previous = null)
	{
		parent::__construct(sprintf($this->message, $cacheName), $code, $previous);
	}
}
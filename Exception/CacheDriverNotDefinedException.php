<?php

namespace Samarties\CacheBundle\Exception;

class CacheDriverNotDefinedException extends \Exception
{
	protected $message = 'The cache driver \'%s\' has not been defined. Have you added it to the drivers list?';
	
	public function __construct($driver, $code = 0, \Exception $previous = null)
	{
		parent::__construct(sprintf($this->message, $driver), $code, $previous);
	}
}
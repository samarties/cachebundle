<?php

namespace Samarties\CacheBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
	/**
	 * @inheritDoc
	 */
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('samarties_cache');

		$rootNode
			->children()				
				->arrayNode('drivers')
					->prototype('scalar')->end()
					->defaultValue(array(
						'xcache' => 'Samarties\Cacher\Driver\XcacheDriver',
					))
				->end()
				->arrayNode('caches')
					->prototype('array')
						->children()
							->scalarNode('id')->defaultFalse()->end()
							->scalarNode('salt')->defaultValue('')->end()
							->booleanNode('enabled')->defaultTrue()->end()
							->scalarNode('driver')->end()
							->booleanNode('doctrine_adapter')->defaultValue(false)->end()
						->end()
					->end()
				->end()
			->end();
		
		return $treeBuilder;
	}
}
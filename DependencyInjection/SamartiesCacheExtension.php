<?php

namespace Samarties\CacheBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Samarties\CacheBundle\Exception\CacheDriverNotDefinedException;

class SamartiesCacheExtension extends Extension
{
	/**
	 * @inheritDoc
	 */
	public function load(array $configs, ContainerBuilder $container)
	{
		$loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
		$loader->load('services.yml');
		
		$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);
		
		$cacheManagerDefinition = $container->getDefinition('samarties.cache.manager');
		foreach($config['caches'] as $cacheName => $cacheConfig)
		{
			if (!in_array($cacheConfig['driver'], array_keys($config['drivers'])))
			{
				throw new CacheDriverNotDefinedException($cacheConfig['driver']);
			}
			
			// create a service for this cache
			$cacheId = $cacheConfig['id'] ? $cacheConfig['id'] : sprintf('samarties.cache.cache.%s', str_replace(' ', '_', $cacheName));
			$cacheDefinition = new Definition('Samarties\CacheBundle\Cache\Cache', array($config['drivers'][$cacheConfig['driver']], $cacheConfig['salt']));
			$cacheDefinition->addMethodCall('setEnabled', array($cacheConfig['enabled']));
			$container->setDefinition($cacheId, $cacheDefinition);
			
			if ($cacheConfig['doctrine_adapter'])
			{
				// create a doctrine adapter for this cache
				$doctrineCacheId = sprintf('samarties.cache.adapter.doctrine.%s', str_replace(' ', '_', $cacheName));
				$doctrineCacheDefinition = new Definition('Samarties\CacheBundle\Adapter\DoctrineAdapter', array());
				$doctrineCacheDefinition->addMethodCall('setCache', array($cacheDefinition));
				$container->setDefinition($doctrineCacheId, $doctrineCacheDefinition);
			}
			
			// store the cache service in our cache manager
			$cacheManagerDefinition->addMethodCall('addCacheService', array($cacheName, $cacheId));
		}
	}
}
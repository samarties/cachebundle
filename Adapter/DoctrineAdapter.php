<?php

namespace Samarties\CacheBundle\Adapter;

use Samarties\CacheBundle\Cache\CacheInterface;
use Doctrine\Common\Cache\CacheProvider;
use Samarties\Cacher\Exception\CacheDataNotFoundException;

class DoctrineAdapter extends CacheProvider
{
	protected $cache;

	/**
	 * @param CacheInterface $cache
	 */
	public function setCache(CacheInterface $cache)
	{
		$this->cache = $cache;
	}
	
	/**
     * @inheritDoc
     */
    protected function doFetch($id)
    {
    	try {
        	$result = $this->cache->fetch($id);
    	} catch (CacheDataNotFoundException $e) {
    		$result = null;
    	}

        return null === $result ? false : $result;
    }

    /**
     * @inheritDoc
     */
    protected function doContains($id)
    {
        return $this->cache->contains($id);
    }

    /**
     * @inheritDoc
     */
    protected function doSave($id, $data, $lifeTime = false)
    {
        return $this->cache->save($id, $data, array('doctrine'), $lifeTime);
    }

    /**
     * @inheritDoc
     */
    protected function doDelete($id)
    {
        return $this->cache->delete($id);
    }

    /**
     * @inheritDoc
     */
    protected function doFlush()
    {
        return $this->cache->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function doGetStats()
    {
        return array(
            Cache::STATS_HITS => false,
            Cache::STATS_MISSES => false,
            Cache::STATS_UPTIME => false,
            Cache::STATS_MEMORY_USAGE => false,
            Cache::STATS_MEMORY_AVAILABLE => false,
        );
    }
}